openapi: 3.1.0
info:
  title: weather
  version: '1.0'
  summary: Weather data per city
servers:
  - url: 'http://localhost:3000'
paths:
  /cities:
    get:
      summary: GET all cities
      tags: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                description: ''
                minItems: 1
                uniqueItems: true
                x-examples:
                  example-1:
                    - name: Budapest
                      timeZone: Europe/Budapest
                      latestForecast:
                        weather: Sunny
                        from: 1
                        to: 2
                      id: 60aa77ff60aa9f0c28baa7c6
                    - name: Debrecen
                      timeZone: Europe/Budapest
                      latestForecast: null
                      id: 60aaa2e3bcc6fb1a30534c3b
                items:
                  type: object
                  properties:
                    name:
                      type: string
                      minLength: 1
                    timeZone:
                      type: string
                      minLength: 1
                    latestForecast:
                      $ref: '#/components/schemas/Forecast'
                    id:
                      type: string
                      minLength: 1
                  required:
                    - name
                    - timeZone
                    - id
              examples:
                example-1:
                  value:
                    - name: Budapest
                      timeZone: Europe/Budapest
                      latestForecast:
                        weather: Sunny
                        from: 1
                        to: 2
                      id: 60aa77ff60aa9f0c28baa7c6
                    - name: Debrecen
                      timeZone: Europe/Budapest
                      latestForecast: null
                      id: 60aaa2e3bcc6fb1a30534c3b
      operationId: get-cities
      description: Get cities data with latest forecast
      parameters:
        - schema:
            type: integer
          in: query
          name: skip
          description: paging - skip this many resources
        - schema:
            type: integer
            default: 50
          in: query
          name: limit
          description: return this many resources
    post:
      summary: ''
      operationId: post-cities
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  name:
                    type: string
                    minLength: 1
                  timeZone:
                    type: string
                    minLength: 1
                  id:
                    type: string
                    minLength: 1
                required:
                  - name
                  - timeZone
                  - id
                x-examples:
                  example-1:
                    name: deleteme4
                    timeZone: Europe/wasd
                    id: 60aae69da5b03b559866d2e0
      requestBody:
        content:
          application/json:
            schema:
              description: ''
              type: object
              properties:
                name:
                  type: string
                  minLength: 1
                timeZone:
                  type: string
                  minLength: 1
              required:
                - name
                - timeZone
              x-examples:
                example-1:
                  name: Budapest
                  timeZone: Europe/Budapest
      description: Create a new city
  '/cities/{cityId}':
    parameters:
      - schema:
          type: string
        name: cityId
        in: path
        required: true
    get:
      summary: GET a city
      tags: []
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                description: ''
                type: object
                x-examples:
                  example-1:
                    name: Budapest
                    timeZone: Europe/Budapest
                    forecasts:
                      - weather: Sunny
                        from: 1
                        to: 2
                      - weather: Sunny
                        from: 1918773556738
                        to: 1918946356738
                      - weather: Sunny
                        from: 1918946356738
                        to: 1918946356738
                    id: 60aa77ff60aa9f0c28baa7c6
                properties:
                  name:
                    type: string
                    minLength: 1
                  timeZone:
                    type: string
                    minLength: 1
                  forecasts:
                    type: array
                    uniqueItems: true
                    minItems: 1
                    items:
                      $ref: '#/components/schemas/Weather'
                  id:
                    type: string
                    minLength: 1
                required:
                  - name
                  - timeZone
                  - forecasts
                  - id
      operationId: get-cities-cityId
      description: Return city with latest forecast
    delete:
      summary: ''
      operationId: delete-cities-cityId
      responses:
        '200':
          description: OK
      description: Deletes city and belonging forecasts
    patch:
      summary: ''
      operationId: patch-cities-cityId
      responses:
        '200':
          description: OK
      description: update city
      requestBody:
        content:
          application/json:
            schema:
              description: ''
              type: object
              x-examples:
                example-1:
                  name: Budapest
                  timeZone: Europe/Budapest
              properties:
                name:
                  type: string
                  minLength: 1
                timeZone:
                  type: string
                  minLength: 1
components:
  schemas:
    User:
      title: User
      type: object
      description: ''
      examples:
        - id: 142
          firstName: Alice
          lastName: Smith
          email: alice.smith@gmail.com
          dateOfBirth: '1997-10-31'
          emailVerified: true
          signUpDate: '2019-08-24'
      properties:
        id:
          type: integer
          description: Unique identifier for the given user.
        firstName:
          type: string
        lastName:
          type: string
        email:
          type: string
          format: email
        dateOfBirth:
          type: string
          format: date
          example: '1997-10-31'
        emailVerified:
          type: boolean
          description: Set to true if the user's email has been verified.
        createDate:
          type: string
          format: date
          description: The date that the user was created.
      required:
        - id
        - firstName
        - lastName
        - email
        - emailVerified
    Weather:
      type: string
      title: Weather
      enum:
        - Sunny
        - Cloudy
        - Stormy
      description: |-
        enum with values: 
        Sunny
        Cloudy
        Stormy
      x-internal: false
    Forecast:
      type: object
      description: 'from, to are epoch time'
      x-examples:
        example-1:
          weather: Sunny
          from: 1918946356738
          to: 1918946356738
      additionalProperties: false
      properties:
        weather:
          $ref: '#/components/schemas/Weather'
        from:
          type: number
        to:
          type: number
      required:
        - weather
        - from
        - to
