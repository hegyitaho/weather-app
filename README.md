# explanation
## The city CRUD operations:

I chose separate collection for the weather data bc there can be huge number of forecasts for a city which would be less efficient to query (e.g. latest by time) as nested array.

There's some API doc for this in reference/weather.yml which was created with [stoplight](https://hegyitaho.stoplight.io/studio/weather-app).

## Create an endpoint that returns the latest forecast added to the system, and the city it was added to.

This is also where the separate collections come handy. I should've also enabled the paging queries *- skip, limit -* which is already there for the generic `GET /cities` endpoint. Right now it shows the latest as required for `GET /cities?sortBy=forecastAdded`.
## Create 2 endpoints where the user can "lock" and "unlock" a city. No changes can be made to a locked city. A locked city can not be deleted. A locked city must be locked for at least 10 minutes, can not be unlocked earlier.

Done.

## Create an endpoint where I can get back the name and ID of every locked city.

Skipped this, would've been something like `GET /cities?fields[]=name` to only return field + id combos with paging.

## Websockets: Create a web socket server where the user could "subscribe" to specific cities, and get forecast changes broadcasted for them.
Couldn't make this work...

## Caching: Cache every http `GET` request response in a Redis server, and serve it as the response without hitting the database if the resource did not change. Be sure to invalidate the cache when necessary.
Done.

## misc
The project's structure is a mess now. I'm still not familiar enough with this framework, but there's too much magic going on with little documentation and not enough resources on the internet. There are no tests bc I was more interested in the tasks, also not quite sure how to do unit level tests here, but wanted to do at least e2e tests if there was more time.

---

# ORIGINAL README
## Node.js

### Rules

- Use the latest Node.js LTS version.
- You can choose the framework you work with, but we recommend [NestJS](https://nestjs.com/)
- We'd prefer if you used [TypeScript](http://typescriptlang.org/).
- You do not have to solve the tasks in order
- You can skip tasks
- You can use any database you'd like.

### Bonus tasks

- Set up docker/docker-compose to make it easy to run the project.
- Create a README file with some explanations about the project.

### Tasks

We are going to create a small weather API that holds weather data about cities.

- Set up a database of your choice. (MongoDB/Postgres/Redis)
- Set up a linter.
- Use [TypeScript](http://typescriptlang.org/).
- Document every endpoint you make with [Swagger](https://swagger.io/)

- Create REST CRUD endpoints for the cities and forecasts. Each city must have a unique ID, a name, a timezone, and a list of forecasts with predicted durations. Example:
```
{
  "id": "7d27e259-15f2-4c3a-9843-ab9babf7c22b",
  "name": "Budapest",
  "timezone": "Europe/Budapest",
  "forecasts": [
    {
      "forecast": "Sunny",
      "from": 1918773556738,
      "to": 1918946356738,
    }
  ]
}
```

In the `forecasts` lists, the `from` property is always a smaller timestamp than then `to` property.

The `forecast` property is an enum. Values might be:
```
Sunny
Cloudy
Stormy
```

The city CRUD operations:

    - Get the list of cities.
        - When listing the cities, only show the latest forecast for each one.
    - Add a new city.
    - Change a city name and timezone.
    - Delete a city.
    - Get one city.
        - This endpoint should give back all the forecasts.
    - Add forecast to a city.
    - Remove latest forecast of a city.

- Create an endpoint that returns the latest forecast added to the system, and the city it was added to.

- Create 2 endpoints where the user can "lock" and "unlock" a city. No changes can be made to a locked city. A locked city can not be deleted. A locked city must be locked for at least 10 minutes, can not be unlocked earlier.

- Create an endpoint where I can get back the name and ID of every locked city.

- Websockets: Create a web socket server where the user could "subscribe" to specific cities, and get forecast changes broadcasted for them.

- Caching: Cache every http `GET` request response in a Redis server, and serve it as the response without hitting the database if the resource did not change. Be sure to invalidate the cache when necessary.


