import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CityModule } from './city/city.module';
import { RedisCacheModule } from './city/cache/redis-cache.module';

const MONGO_URI_FROM_SECRET =
  'mongodb+srv://admin:VArzZWKX6lciZnPE@cluster0.tyeqa.mongodb.net/weather?retryWrites=true&w=majority';
@Module({
  imports: [
    MongooseModule.forRoot(MONGO_URI_FROM_SECRET, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    }),
    CityModule,
    RedisCacheModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [RedisCacheModule],
})
export class AppModule {}
