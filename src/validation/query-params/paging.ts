import { IsInt, IsOptional } from 'class-validator';

export class PagingParams {
  @IsInt()
  @IsOptional()
  limit?: number;

  @IsInt()
  @IsOptional()
  skip?: number;
}
