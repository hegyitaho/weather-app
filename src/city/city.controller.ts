import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  Query,
  ClassSerializerInterceptor,
  UseInterceptors,
} from '@nestjs/common';
import { CityService } from './city.service';
import { CreateCityDto } from './dto/create-city.dto';
import { UpdateCityDto } from './dto/update-city.dto';
import { CreateForecastDto } from './dto/create-forecast.dto';
import { MongoIdParam } from '../validation/query-params/mongo-id-param';
import { PagingParams } from '../validation/query-params/paging';
import { City, CityDocument } from './schemas/city.schema';
import {
  CitiesSortingParams,
  CitySortBy,
  OrderBy,
  assertCityFound,
} from './city.utils';
import { RedisCacheService } from './cache/redis-cache.service';
import { Logger } from '@nestjs/common';
import { Forecast } from './schemas/forecast.schema';

@Controller('cities')
export class CityController {
  private readonly logger = new Logger('CityGateWay');

  constructor(
    private readonly cacheManager: RedisCacheService,
    private readonly cityService: CityService,
  ) {}

  @Post()
  async create(@Body() createCityDto: CreateCityDto) {
    const { _id, name, timeZone } = await this.cityService.create(
      createCityDto,
    );
    return new City({ _id, name, timeZone });
  }

  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  findAll(@Query() params: PagingParams & CitiesSortingParams) {
    const forecastSort = getSortParamsForForecast(params);
    return forecastSort
      ? this.cityService.getLatestForecast(forecastSort).then((city) => [city])
      : this.cityService.findAll(params);
  }

  @Get(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  async findOne(@Param() params: MongoIdParam) {
    const cacheKey = cityCacheKey(params);
    const cachedResult = (await this.cacheManager.get(
      cacheKey,
    )) as CityDocument;

    if (cachedResult) {
      this.logger.log(`hit cache for ${cacheKey}`);
      return new City({
        ...cachedResult,
        forecasts: cachedResult.forecasts.map(
          (f) =>
            new Forecast({
              ...f,
              to: new Date(f.to),
              from: new Date(f.from),
            }),
        ),
      });
    }

    const result = await this.cityService.findOne(params.id);
    assertCityFound(result);
    this.cacheManager
      .set(`get-city-${params.id}`, result)
      .then(() => this.logger.log(`saving cache for ${cacheKey}`));

    return new City(result);
  }

  @Patch(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  async update(
    @Param() params: MongoIdParam,
    @Body() updateCityDto: UpdateCityDto,
  ) {
    await this.cacheManager.del(cityCacheKey(params));
    const updatedCity = await this.cityService.update(params.id, updateCityDto);
    return new City(updatedCity);
  }

  @Delete(':id')
  async remove(@Param() params: MongoIdParam) {
    await this.cacheManager.del(cityCacheKey(params));
    await this.cityService.remove(params.id);
  }

  @Post(':id/lock')
  @UseInterceptors(ClassSerializerInterceptor)
  async lockCity(@Param() params: MongoIdParam) {
    await this.cityService.lockCity(params.id);
  }

  @Post(':id/unlock')
  @UseInterceptors(ClassSerializerInterceptor)
  async unlockCity(@Param() params: MongoIdParam) {
    await this.cityService.unlockCity(params.id);
  }

  @Post(':id/forecast')
  async addForecastToCity(
    @Param() params: MongoIdParam,
    @Body() createForecastDto: CreateForecastDto,
  ) {
    await this.cacheManager.del(cityCacheKey(params));
    const result = await this.cityService.findOne(params.id);
    assertCityFound(result);
    assertDurationPositive(createForecastDto);
    await this.cityService.createForecast(params.id, createForecastDto);
  }

  @Delete(':id/forecast')
  async removeLastForecastFromCity(@Param() params: MongoIdParam) {
    await this.cacheManager.del(cityCacheKey(params));
    const result = await this.cityService.findOne(params.id);
    assertCityFound(result);
    await this.cityService.removeForecast(params.id);
  }
}

function cityCacheKey(params: MongoIdParam) {
  return `city-${params.id}`;
}

function assertDurationPositive({ from, to }) {
  if (from >= to) {
    throw new HttpException(
      `from: ${from} must be smaller than to: ${to}`,
      HttpStatus.BAD_REQUEST,
    );
  }
}

function getSortParamsForForecast({
  sortBy,
  orderBy,
}: any): Record<string, number> {
  return {
    [CitySortBy.FORECASTADDED]: {
      createdAt: orderBy === OrderBy.ASCENDING ? 1 : -1,
    },
  }[sortBy];
}
