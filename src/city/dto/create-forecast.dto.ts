import { Weather } from '../schemas/forecast.schema';
import { IsInt, IsEnum } from 'class-validator';

export class CreateForecastDto {
  @IsEnum(Weather)
  weather: Weather;

  @IsInt()
  from: number;

  @IsInt()
  to: number;
}
