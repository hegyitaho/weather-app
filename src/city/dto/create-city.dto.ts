export class CreateCityDto {
  name: string;
  timeZone: string;
}
