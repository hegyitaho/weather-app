import { Module } from '@nestjs/common';
import { CityService } from './city.service';
import { CityController } from './city.controller';
import { ForecastSchema, Forecast } from './schemas/forecast.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { City, CitySchema } from 'src/city/schemas/city.schema';
import { CityGateway } from './city.gateway';
import { RedisCacheModule } from './cache/redis-cache.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: City.name, schema: CitySchema },
      { name: Forecast.name, schema: ForecastSchema },
    ]),
    RedisCacheModule,
  ],
  controllers: [CityController],
  providers: [CityService, CityGateway],
})
export class CityModule {}
