import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Logger, Injectable, HttpException } from '@nestjs/common';
import mongoose, { Connection, Model } from 'mongoose';
import { DateTime } from 'luxon';
import { pickBy, isNil, complement } from 'ramda';

import { CreateCityDto } from './dto/create-city.dto';
import { UpdateCityDto } from './dto/update-city.dto';
import { City, CityDocument } from 'src/city/schemas/city.schema';
import {
  Forecast,
  ForecastDocument,
  ForecastChange,
} from './schemas/forecast.schema';
import { CreateForecastDto } from './dto/create-forecast.dto';
import { assertCityFound } from './city.utils';
import { Subject } from 'rxjs';

const HttpStatus_LOCKED = 423;
const LOCK_MINIMUM_IN_MINUTES = 10;
@Injectable()
export class CityService {
  private readonly logger = new Logger(CityService.name);
  private readonly forecastChangeTrigger = new Subject<ForecastChange>();
  readonly forecastChange = this.forecastChangeTrigger.asObservable();

  constructor(
    @InjectModel(City.name) private cityModel: Model<CityDocument>,
    @InjectModel(Forecast.name) private forecastModel: Model<ForecastDocument>,
    @InjectConnection() private connection: Connection,
  ) {}

  async create(createCityDto: CreateCityDto) {
    try {
      return await this.cityModel.create(createCityDto);
    } catch (error) {
      this.logger.error(
        `failed to create city: ${{ createCityDto }} - ${error.message}`,
        error.stack,
      );
      throw error;
    }
  }

  async findAll({ skip = 0, limit = 50 } = {} as any): Promise<City[]> {
    const cities = await this.cityModel
      .find({}, null, { skip, limit })
      .lean()
      .exec();

    const latestForecastForEachCity =
      await this.forecastModel.aggregate<ForecastDocument>([
        { $sort: { city: 1, from: -1 } },
        {
          $group: {
            _id: '$city',
            doc: { $first: '$$ROOT' },
          },
        },
        { $replaceRoot: { newRoot: '$doc' } },
      ]);

    return cities
      .map((city) => ({
        ...city,
        forecasts: undefined,
        latestForecast: new Forecast(
          latestForecastForEachCity.find(
            (forecast) => forecast.city.toString() === city._id.toString(),
          ),
        ),
      }))
      .map((a) => new City(a));
  }

  async getLatestForecast(sort) {
    const latestForecast = await this.forecastModel
      .findOne({}, null, { sort })
      .populate(['city'])
      .lean()
      .exec();

    return new City({
      ...latestForecast.city,
      forecasts: undefined,
      latestForecast: new Forecast(latestForecast),
    });
  }

  async findOne(id: string) {
    const [city, forecasts] = await Promise.all([
      this.cityModel.findById(id).lean().exec(),
      this.forecastModel
        .find({ city: id })
        .sort({ city: 1, from: -1 })
        .lean()
        .exec(),
    ]);
    return !city
      ? null
      : {
          ...city,
          forecasts: forecasts.map((f) => new Forecast(f)),
        };
  }

  async update(id: string, updateCityDto: UpdateCityDto) {
    const city = await this.findCityById(id);
    setFieldsForMongooseDocument(city, updateCityDto);
    return (await city.save()).toObject();
  }

  remove(id: string) {
    return this.connection.transaction(async (session) => {
      const [city] = await Promise.all([
        this.cityModel.findByIdAndDelete(id, { session }),
        this.forecastModel.deleteMany({ city: id }, { session }),
      ]);
      assertCityFound(city);
      assertCityUnlocked(city);
    });
  }

  async lockCity(id: string) {
    const city = await this.findCityById(id);
    setFieldsForMongooseDocument(city, {
      lock: { lockedTime: new Date(), isLocked: true },
    });
    return (await city.save()).toObject();
  }

  async unlockCity(id: string) {
    const city = await this.cityModel.findById(id);
    assertCityFound(city);
    assertCityLockedTimePassed(city);
    city.lock.isLocked = false;
    return (await city.save()).toObject();
  }

  async createForecast(cityId: string, createForecastDto: CreateForecastDto) {
    const createdForecast = await this.forecastModel.create({
      city: cityId,
      ...createForecastDto,
    });
    this.forecastChangeTrigger.next({
      change: 'new',
      ...new Forecast(createdForecast),
    });
    return createdForecast;
  }

  async removeForecast(cityId: string) {
    const deletedforecast = await this.forecastModel.findOneAndDelete(
      { city: cityId },
      { sort: { city: 1, from: -1 } },
    );
    this.forecastChangeTrigger.next({
      change: 'deleted',
      ...new Forecast(deletedforecast),
    });
    return deletedforecast;
  }

  async findCityById(id: string) {
    const city = await this.cityModel.findById(id);
    assertCityFound(city);
    assertCityUnlocked(city);
    return city;
  }
}

function setFieldsForMongooseDocument<T extends mongoose.Document>(
  city: T,
  update: any,
) {
  Object.assign(city, pickBy(complement(isNil), update));
}

function assertCityUnlocked(city: CityDocument) {
  if (city.lock.isLocked) {
    throw new HttpException('City is locked from changes', HttpStatus_LOCKED);
  }
}

function assertCityLockedTimePassed(city: CityDocument) {
  const waitUntilUnlockTime = DateTime.fromJSDate(city.lock.lockedTime).plus({
    minutes: LOCK_MINIMUM_IN_MINUTES,
  });
  if (waitUntilUnlockTime > DateTime.now()) {
    throw new HttpException(
      `City is locked from changes before ${waitUntilUnlockTime}`,
      HttpStatus_LOCKED,
    );
  }
}
