import { IsOptional } from 'class-validator';
import { HttpException, HttpStatus } from '@nestjs/common';
export class CitiesSortingParams {
  @IsOptional()
  sortBy?: CitySortBy;

  @IsOptional()
  orderBy?: OrderBy;
}

export enum CitySortBy {
  FORECASTADDED = 'forecastAdded',
}

export enum OrderBy {
  ASCENDING = 'asc',
  DESCENDING = 'desc',
}

export function assertCityFound(result) {
  if (!result) {
    throw new HttpException('City not found', HttpStatus.NOT_FOUND);
  }
}
