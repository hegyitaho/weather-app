import {
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WsResponse,
} from '@nestjs/websockets';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ForecastChange } from './schemas/forecast.schema';
import { CityService } from './city.service';
import { Logger } from '@nestjs/common';

@WebSocketGateway()
export class CityGateway
  implements OnGatewayConnection, OnGatewayInit, OnGatewayDisconnect
{
  private readonly logger = new Logger('CityGateWay');
  constructor(private readonly cityService: CityService) {}

  handleConnection(client) {
    client.send(JSON.stringify({ event: 'connected', data: true }));
  }

  handleDisconnect(client) {
    client.send(JSON.stringify({ event: 'disconnected', data: true }));
  }

  afterInit() {
    this.logger.log('gateway init');
  }

  @SubscribeMessage('cities')
  handleEvent(
    @MessageBody() id: string,
  ): Observable<WsResponse<ForecastChange>> {
    return this.cityService.forecastChange.pipe(
      filter(isSubscribedCity(id)),
      map((forecastWeatherChange) => ({
        event: 'cities',
        data: forecastWeatherChange,
      })),
    );
  }
}

function isSubscribedCity(
  id: string,
): (value: ForecastChange, index: number) => boolean {
  return (forecastWeatherChange) =>
    forecastWeatherChange.city.toHexString() === id;
}
