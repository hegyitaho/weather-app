import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { BaseDBObject } from './base-db.schema';
import { Exclude, Transform } from 'class-transformer';
import { PartialType } from '@nestjs/mapped-types';

export type ForecastDocument = Forecast & mongoose.Document;

export enum Weather {
  SUNNY = 'sunny',
  CLOUDY = 'cloudy',
  STORMY = 'stormy',
}

@Schema({ timestamps: true })
export class Forecast extends BaseDBObject {
  @Prop({ required: true, enum: Object.values(Weather) })
  weather: string;

  @Prop({ required: true })
  @Transform(transformDateToEpoch, { toPlainOnly: true })
  from: Date;

  @Transform(transformDateToEpoch, { toPlainOnly: true })
  @Prop({ required: true })
  to: Date;

  @Exclude()
  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'City' })
  city: mongoose.Types.ObjectId;

  @Exclude()
  get id() {
    return undefined;
  }

  constructor(partial: Partial<Forecast> = {}) {
    super();
    Object.assign(this, partial);
  }
}

export class ForecastChange extends PartialType(Forecast) {
  change: 'new' | 'deleted';
}

export const ForecastSchema = SchemaFactory.createForClass(Forecast);
ForecastSchema.index({ city: 1, from: -1 }, { unique: true });
ForecastSchema.index({ createdAt: 1 });

function transformDateToEpoch({ value }) {
  return (value as Date).getTime();
}
