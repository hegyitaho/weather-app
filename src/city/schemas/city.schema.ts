import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, LeanDocument } from 'mongoose';
import { BaseDBObject } from './base-db.schema';
import { Forecast } from './forecast.schema';
import { Transform, Exclude } from 'class-transformer';
import { Lock, LockSchema } from './lock.schema';

export type CityDocument = City & Document;

@Schema({ timestamps: true })
export class City extends BaseDBObject {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  timeZone: string;

  @Transform(({ value }) => ((value as Forecast).city ? value : null), {
    toPlainOnly: true,
  })
  latestForecast?: Forecast;

  @Transform(
    ({ value }) => value && ((value as Forecast[]).length ? value : []),
    {
      toPlainOnly: true,
    },
  )
  forecasts?: Forecast[] | LeanDocument<Forecast>[];

  @Exclude()
  @Prop({ type: LockSchema, default: () => new Lock() })
  lock?: Lock;

  constructor(partial: Partial<City> = {}) {
    super();
    Object.assign(this, partial);
  }
}

export const CitySchema = SchemaFactory.createForClass(City);

CitySchema.index(
  { name: 1, timeZone: 1 },
  { unique: true, collation: { locale: 'en' } },
);
