import { classToPlain } from 'class-transformer';
import { Expose, Exclude } from 'class-transformer';
import * as mongoose from 'mongoose';

@Exclude()
export class BaseDBObject {
  @Expose()
  get id() {
    try {
      return this._id.toHexString();
    } catch (e) {
      this._id;
    }
  }

  @Exclude()
  _id?: mongoose.Types.ObjectId;

  @Exclude()
  __v?: number;

  @Exclude()
  createdAt?: Date;

  @Exclude()
  updatedAt?: Date;

  toJSON() {
    return classToPlain(this);
  }

  toString() {
    return JSON.stringify(this.toJSON());
  }
}
