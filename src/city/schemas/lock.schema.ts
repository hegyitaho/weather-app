import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class Lock {
  @Prop({ default: new Date(0) })
  lockedTime?: Date;

  @Prop({ default: false })
  isLocked?: boolean;
}
export const LockSchema = SchemaFactory.createForClass(Lock);
