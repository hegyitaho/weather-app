import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  ValidationError,
} from '@nestjs/common';
import { Response } from 'express';
import { MongoError } from 'mongodb';
import { HttpStatus } from '@nestjs/common';

@Catch(MongoError)
export class ValidationExceptionFilter implements ExceptionFilter {
  catch(error: ValidationError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    console.log({ error });
    response.status(getStatusForMongoErrorCode(error)).json({
      message: error.toString(),
    });
  }
}

function getStatusForMongoErrorCode({ code }: any) {
  return (
    { 11000: HttpStatus.CONFLICT }[code] || HttpStatus.INTERNAL_SERVER_ERROR
  );
}
