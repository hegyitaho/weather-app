import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { MongoError } from 'mongodb';
import { HttpStatus } from '@nestjs/common';

@Catch(MongoError)
export class MongoErrorFilter implements ExceptionFilter {
  catch(error: MongoError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(getStatusForMongoErrorCode(error)).json({
      message: error.message,
    });
  }
}

function getStatusForMongoErrorCode({ code }: any) {
  return (
    { 11000: HttpStatus.CONFLICT }[code] || HttpStatus.INTERNAL_SERVER_ERROR
  );
}
